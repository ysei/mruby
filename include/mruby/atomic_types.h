#ifndef MRUBY_ATOMIC_TYPES_H
#define MRUBY_ATOMIC_TYPES_H

typedef mrb_int   mrb_atomic_t;
typedef mrb_bool  mrb_atomic_bool_t;
typedef int32_t   mrb_atomic_i32_t;
typedef uintptr_t mrb_atomic_ptr_t;

#endif

